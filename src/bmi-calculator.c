/*
 * bmi-calculator.c - calculate your BMI
 * Copyright (C) 2018  Tom Ient <tomient@posteo.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void usage(FILE *stream, int exitcode, char progname[])
{
	fprintf(stream, "Usage: %s HEIGHT/m MASS/kg\n", progname);
	exit(exitcode);
}

int main(int argc, char *argv[])
{

	if (argc != 3)
		usage(stderr, 1, argv[0]);
	if (strncmp(argv[1], "-h", 2) == 0 || strncmp(argv[1], "--help", 6) == 0)
		usage(stdout, 0, argv[0]);

	double height;
	double mass;
	double bmi;

	height = strtod(argv[1], NULL);
	mass = strtod(argv[2], NULL);

	bmi = mass / (height * height);
	printf("%.1f kg/m^2\n", bmi);

	return 0;
}
